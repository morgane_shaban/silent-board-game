# Silent Board Game  <br> MDEF _ Challenge 01 _ (16/02/21 - 19/02/21)

### Ines Burdiles, Morgane Shaban

This is the documentation, description and reflections of Ines and Morgane's Fabacademy microchallenge 01, where they developed a prototype of the Silent Board Game.

![SBGillustration](images/SBGeillustration.jpg)

*Silent Board Game Illustration*


#### What is the Silent Board Game?
The Silent Game was originally created by John Habracken and Mark D Gross, in the context of Design Research in the Department of Architecture at MIT. The game was originally published in 1987 the book Concept Design Games by the same authors. For the first microchallenge of MDEF's Fabacademy, Morgane and Ines developed a version of the board, pieces and instructions, that would enable two people to play the game, developing a space of implicit communication through the silent alternative creation of patterns, sculptures or images. The original version of the game asks to have pieces of different shape, size and colour. For this reason we are using transparent acrylic, plywood, and the acrylic negative is used as a "generator" for casting biomaterial pieces of different colours. This "generator" is a part of the board, so in case any piece is lost, it can easily be replaced.

![SBGoriginal](images/SBGoriginal.jpg)

*original Silent Game by John Habracken and Mark D Gross, 1987*


#### The game rules
- 0. Set up the game as shown above.
- 1. Player 1 places pieces on the canvas (any amount of pieces).
- 2. Player 2 observes.
- 3. Once player 1 is finished, player 2 follows players 1’s pattern by adding more pieces to the canvas.
- 4. Player 2 then can choose to add a new pattern by adding more pieces again.
- 5. Player 1 observes.
- 6. Once player 2 is finished, player 1 follows players 2's pattern.
- 7. Player 1 then can choose to add a new pattern.
- 8. The game continues (repeat steps 02-07)
- 9. The game is finished only when the 2 players agree they are finished, or once there are no more pieces.
- 10. Discuss your collective pattern, your thoughts and reflections.
Have fun and be creative.

![iterative sketching](images/itterativesketching.jpg)

*iterative sketching*

![Rhino Model](images/rhinomodel.jpg)

*Rhino model iteration*


#### Materials needed
- A 900 x 320 mm board of 4,2mm plywood (or any kind of wood board you have in hand)
- A 300 x 140 mm board of 5,0mm acrylic, transparent (or any colour you have in hand)
- A 600 x 300 mm piece of cotton canvas fabric
- For a batch of biomaterial pieces: 200ml of water, 40g of gelatine, and any natural dye (e.g. we used turmeric for yellow, or the dyed water from boiling crushed avocado pits) You can also add some drops of essential oil to avoid mould and have a nice smell


#### Fabrication process of laser cutting
Various laser cutters were used for the fabrication process, one specifically for each material used, in this order.
- The Trotec Speedy 400 for cutting the 5,0mm acrylic. With the parameters:
Power : 90
Speed : 0.5
Hz : 20000
- The Multicam 2000 Laser for cutting the 4,2mm plywood pieces and box. With the parameters:
Cut pressure : 3
Speed : 25
Power : 200
- The Trotec Speedy 400 for cutting the canvas fabric. With the parameters:
Power : 35
Speed : 2
Hz : 1000
- The Trotec Speedy 100R for engraving the fabric. With the parameters:
Power : 35
Speed : 100
Hz : 1000

Tips: When making a press fit box, remember to do kerf tests

![SBGkerftests](images/kerftest.jpg)

*laser cut kerf tests*

![lasercutting](images/lasercutting.jpg)

*laser cutting plywood*

#### Fabrication process of Biomaterial pieces
You will need the following tools and appliances:
- small cooking pot
- cooking stove
- spoon
- 200ml of water
- 40gr of gelatine
- half a small spoon of natural dye like turmeric
- essential oil like rosemary
- an A4 acrylic board
- the acrylic "generator" mould
Instructions : First prepare the acrylic generator by wrapping the border to the A4 acrylic board with duct tape, once it is completely wrapped to avoid any leaking. Then you can start the cooking. Put the pan over the stove, add water and heat to medium temperature, add the essential oil and the dye, stir until the water has gained an even colour. Then slowly start adding the gelatine as you constantly stir so the gelatine is completely dissolved. When you have added and dissolved all the gelatine, rise the temperature to leave to a boil. When the mixture has boiled turn off the stove and let it cool for a few minutes by stirring, always avoiding bubbles. You must have the acrylic generator prepared to receive the mixture.

![cooking biomaterials](images/cooking.jpg)

*cooking biomaterial pieces*

#### Future Developments
The Silent Board Game acts as a prototype which has much potential. First we can experiment further with different shapes, scale and materials of the pieces. Another option is that the game can simply be the box, instructions and canvas board and then the players are free to make or find the pieces they play with. There also the possibility of scaling the game up and making it into an installation project where you move human scaled objects and play with them and your body to make sculptures images and performances. The next step is to test the silent game in different contexts and with people. Ines and Morgane will be working with schools to test the silent board game further, letting the game and pieces evolve and adapt.

##### (to find out more and keep updated on this project find us on instagram @ hybrid_play  @ revealedmatter)

![playing](images/SBG.GIF) ![playing](images/SBGplay.GIF)

*Silent Board Game*
*play in progress*


#### What you will find in this repo
- 3D Rhino model files
- Laser cut files and process, of the box, board, rules and pieces (see details above)
- Rules of the game
- Kerf test file (for pressfit box)
- Images of the process

#### Downloads
- Board lasercut Cotton Canvas Fabric.3dm
- Box and Pieces 3D Model.3dm
- Box and Pieces Lasercut Plywood 4mm.3dm
- Box Kerf Test Lasercut Plywood 4mm.3dm
- Pieces Generator Lasercut Acrylic 5mm.3dm
- Silent Board Game Rules Engrave.png
- Silent Board Game Rules.ai

#### Links to personal Posts
- Morgane Sha'ban : https://morgane_shaban.gitlab.io/mdef/challenge01.html
- Inés Burdiles : https://ines_burdiles.gitlab.io/sitio/single-fabweek4.html
